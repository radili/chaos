﻿/* Crowd Simulator Engine
** Copyright (C) 2018 - Inria Rennes - Rainbow - Julien Pettre
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**
** Authors: Julien Bruneau, Tristan Le Bouffant
**
** Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMvt : MonoBehaviour
{


#region attributes
    // Rotation - Looking at an agent
    public int lookAt_Id;                       // Id of the agent to look at
    private GameObject lookAt_Agent = null;     // Agent to look at

    // Translation - Following an agent
    public int follow_Id;                       // Id of the agent to follow
    private GameObject follow_Agent;            // Agent to follow
    private Vector3 follow_LastPosition;        // Last position of the agent to follow
    public bool follow_LockX;                   // Lock axe X of the camera during camera's translation
    public bool follow_LockZ;                   // Lock axe Z of the camera during camera's translation
    public bool follow_LockY;                   // Lock axe Z of the camera during camera's translation
#endregion

    // Use this for initialization
    void Start()
    {
    }

    /// <summary>
    /// Update the target of the camera movement
    /// </summary>
    public void updateTargetedObject()
    {
        GameObject[] list = GameObject.FindGameObjectsWithTag("Player");


        if (lookAt_Id >= 0 && list.Length > 0)
        {
            if (lookAt_Id > list.Length - 1)
                lookAt_Id = list.Length - 1;
            lookAt_Agent = list[lookAt_Id];
        }
        if (follow_Id >= 0 && list.Length > 0)
        {
            if (follow_Id > list.Length - 1)
                follow_Id = list.Length - 1;
            follow_Agent = list[follow_Id];
            follow_LastPosition = follow_Agent.transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
#region ROTATION
        // ------------------------------
        // ROTATION - LOOKING AT AN AGENT
        if (lookAt_Agent != null)
            transform.LookAt(lookAt_Agent.transform);
#endregion

#region TRANSLATION
        // --------------------------------
        // TRANSLATION - FOLLOWING AN AGENT
        if (follow_Agent != null)
        {
            Vector3 delta = follow_Agent.transform.position - follow_LastPosition;
            follow_LastPosition = follow_Agent.transform.position;
            if (follow_LockX)
                delta.x = 0;
            if (follow_LockZ)
                delta.z = 0;
            if (follow_LockY)
                delta.y = 0;
            transform.position = transform.position + delta;
        }
#endregion
    }
}
