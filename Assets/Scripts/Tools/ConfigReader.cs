﻿/* Crowd Simulator Engine
** Copyright (C) 2018 - Inria Rennes - Rainbow - Julien Pettre
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**
** Authors: Julien Bruneau
**
** Contact: crowd_group@inria.fr
*/

using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Collections.Generic;

/// <summary>
/// Static class used to load the simulation parameters from the Config.xml file.
/// </summary>
public static class ConfigReader
{
    #region privateStaticAttributs
    static string configFileName;
    static ConfigData data;         // Config data loaded from xml file
    #endregion

    /// <summary> 
    /// Manage configuration: Load XML file or create default one
    /// </summary>
    static ConfigReader()
    {
        string pathPlayer = Application.dataPath;
        int lastIndex = pathPlayer.LastIndexOf('/');
        string dataPath = pathPlayer.Remove(lastIndex, pathPlayer.Length - lastIndex);

        configFileName = dataPath + @" / Config.xml";
        data = new ConfigData();
    }

    /// <summary>
    /// Load a scenario file
    /// </summary>
    /// <param name="path">Path of the scenario file to load</param>
    static public void LoadConfig(string path)
    {
        configFileName = path;

        if (File.Exists(path))
        {
            data = XMLLoader.LoadXML<ConfigData>(path);
        }
    }

    /// <summary>
    /// Save current parameters in a scenario file
    /// </summary>
    /// <param name="path">Path of the scenario file to save</param>
    public static void SaveConfig(string path)
    {
        XMLLoader.CreateXML<ConfigData>(path, data);
    }

    /// <summary>
    /// Create a template config with all possible parameters
    /// </summary>
    public static void CreateTemplate()
    {
        string pathPlayer = Application.dataPath;
        int lastIndex = pathPlayer.LastIndexOf('/');
        string dataPath = pathPlayer.Remove(lastIndex, pathPlayer.Length - lastIndex);

        ConfigData template = new ConfigData();
        template.colorList = new List<ConfigAgentColor>();
        template.colorList.Add(new ConfigAgentColor(0, 1, 191, 1, 0, 0));
        template.colorList.Add(new ConfigAgentColor(192, 1, 383, 0, 1, 0));
        XMLLoader.CreateXML<ConfigData>(dataPath + @" / ConfigTemplate.xml", template);
    }

    #region get_set
    /// <summary>
    /// Folder of the trajectory files
    /// </summary>
    static public string trajectoriesDir
    {
        get { return data.env_filesPath; }
    }
    /// <summary>
    /// Data about an AssetBundle to load
    /// </summary>
    static public ConfigStage stageInfos
    {
        get { return data.env_stageInfos; }
    }
    /// <summary>
    /// Path to the file containing the obstacles definition
    /// </summary>
    static public string obstaclesFile
    {
        get { return data.env_obstFile; }
    }
    /// <summary>
    /// Starting camera position
    /// </summary>
    static public Vector3 camPosition
    {
        get { return data.cam.position.vect; }
        set { data.cam.position = new ConfigVect3(value); }
    }
    /// <summary>
    /// Starting camera rotation
    /// </summary>
    static public Vector3 camRotation
    {
        get { return data.cam.rotation.vect; }
        set { data.cam.rotation = new ConfigVect3(value); }
    }
    /// <summary>
    /// ID of the agent to look at
    /// </summary>
    static public int camLookAtTarget
    {
        get { return data.cam.lookAtAgent == null ? -1 : data.cam.lookAtAgent.id; }
    }
    /// <summary>
    /// ID of the agent to follow with the camera
    /// </summary>
    static public int camFollowTarget
    {
        get { return data.cam.followAgent == null ? -1 : data.cam.followAgent.id; }
    }
    /// <summary>
    /// Boolean, true if camera follow an agent's translation of the axe X
    /// </summary>
    static public bool camFollowOnX
    {
        get { return data.cam.followAgent == null ? false : data.cam.followAgent.followX; }
    }
    /// <summary>
    /// Boolean, true if camera follow an agent's translation of the axe Y
    /// </summary>
    static public bool camFollowOnY
    {
        get { return data.cam.followAgent == null ? false : data.cam.followAgent.followY; }
    }
    /// <summary>
    /// Boolean, true if the animation should be recorded
    /// </summary>
    static public bool recording
    {
        get { return data.recording.end > data.recording.start; }
    }
    /// <summary>
    /// time to start the recording of the animation
    /// </summary>
    static public float recordingStart
    {
        get { return data.recording.start; }
    }
    /// <summary>
    /// Time to stop the recording of the animation
    /// </summary>
    static public float recordingEnd
    {
        get { return data.recording.end; }
    }
    /// <summary>
    /// Framerate used for the recording of the animation
    /// </summary>
    static public int recordingFramerate
    {
        get { return data.recording.framerate; }
    }
    /// <summary>
    /// Folder where all the images from the animation are recorded
    /// </summary>
    static public string recordingSaveDir
    {
        get { return data.recording.saveDir; }
    }

    /// <summary>
    /// Indicate if body bounding boxes should be recorded
    /// </summary>
    static public bool recordOriginalImg
    {
        get { return data.recording.savedData.ImgOriginal; }
    }
    /// <summary>
    /// Indicate if body bounding boxes should be recorded
    /// </summary>
    static public bool recordSegmentationImg
    {
        get { return data.recording.savedData.ImgSegmentation; }
    }
    /// <summary>
    /// Indicate if body bounding boxes should be recorded
    /// </summary>
    static public bool recordLayeredSegImg
    {
        get { return data.recording.savedData.ImgCategories; }
    }
    /// <summary>
    /// Indicate if body bounding boxes should be recorded
    /// </summary>
    static public bool recordDepthImg
    {
        get { return data.recording.savedData.ImgDepth; }
    }
    /// <summary>
    /// Indicate if body bounding boxes should be recorded
    /// </summary>
    static public bool recordNormalsImg
    {
        get { return data.recording.savedData.ImgNormals; }
    }
    /// <summary>
    /// Indicate if body bounding boxes should be recorded
    /// </summary>
    static public bool recordOpticalFlowImg
    {
        get { return data.recording.savedData.ImgOpticalFlow; }
    }

    /// <summary>
    /// Indicate if body bounding boxes should be recorded
    /// </summary>
    static public bool recordBodyBoudingBoxes
    {
        get { return data.recording.savedData.BodyBoundingBoxes; }
    }

    /// <summary>
    /// Indicate if head bounding boxes should be recorded
    /// </summary>
    static public bool recordHeadBoundingBoxes
    {
        get { return data.recording.savedData.HeadBoundingBoxes; }
    }
    
    /// <summary>
    /// List of colors for the agents
    /// </summary>
    static public List<ConfigAgentColor> agentsColor
    {
        get { return data.colorList; }
    }
    #endregion

}


#region XMLConfigClasses
/// <summary>
/// Main config class to be serialize in XML config
/// </summary>
public class ConfigData
{

    // Evironnement config
    public string env_filesPath;
    public string env_obstFile;
    public ConfigStage env_stageInfos;

    // Camera config
    public ConfigCam cam;


    // Record config
    public ConfigRecording recording;

    // Agent color setup
    [XmlArray("AgentColorList"), XmlArrayItem("color")]
    public List<ConfigAgentColor> colorList;

    public ConfigData()
    {
        // Evironnement config
        env_filesPath = ".\\TrajExample\\ExampleTwoColor\\";
        env_obstFile = "";
        env_stageInfos = new ConfigStage();

        // Camera config
        cam = new ConfigCam();

        // Record config
        recording = new ConfigRecording();

        // Agent color
        //colorList = new List<ConfigAgentColor>();
        //colorList.Add(new ConfigAgentColor(0, 1, 191, 1, 0, 0));
        //colorList.Add(new ConfigAgentColor(192, 1, 383, 0, 1, 0));
    }


}

/// <summary>
/// Parameters of a stage to load from an AssetBundle
/// </summary>
public class ConfigStage
{
    [XmlAttribute]
    public string stageName;
    public string file;
    public ConfigVect3 position;
    public ConfigVect3 rotation;

    public ConfigStage()
    {
        stageName = "";
        file = "";
        position = new ConfigVect3();
        rotation = new ConfigVect3();
    }
}

/// <summary>
/// Camera configuraton to be serialize in XML config
/// </summary>
public class ConfigCam
{
    public ConfigVect3 position;
    public ConfigVect3 rotation;

    public ConfigCamBehaviour1 lookAtAgent;
    public ConfigCamBehaviour2 followAgent;

    public ConfigCam()
    {
        position = new ConfigVect3();
        rotation = new ConfigVect3();
        lookAtAgent = new ConfigCamBehaviour1();
        followAgent = new ConfigCamBehaviour2();
    }
}

/// <summary>
/// Vector 3D that can be serialize in XML config
/// </summary>
public class ConfigVect3
{
    [XmlAttribute]
    public float x;
    [XmlAttribute]
    public float y;
    [XmlAttribute]
    public float z;

    public ConfigVect3()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    public ConfigVect3(Vector3 vect)
    {
        x = vect.x;
        y = vect.z;
        z = vect.y;
    }

    public Vector3 vect
    {
        get { return new Vector3(x, z, y); }
    }
}

/// <summary>
/// Camera behavior configuration to be serialize in XML config
/// </summary>
public class ConfigCamBehaviour1
{
    [XmlAttribute("agentID")]
    public int id;

    public ConfigCamBehaviour1()
    {
        id = -1;
    }
}

/// <summary>
/// Camera behavior configuration to be serialize in XML config
/// </summary>
public class ConfigCamBehaviour2
{
    [XmlAttribute("agentID")]
    public int id;
    [XmlAttribute("followOnX")]
    public bool followX;
    [XmlAttribute("followOnY")]
    public bool followY;

    public ConfigCamBehaviour2()
    {
        id = -1;
        followX = false;
        followY = false;
    }
}

/// <summary>
/// Recording configuration to be serialize in XML config
/// </summary>
public class ConfigRecording
{
    [XmlAttribute]
    public float start;
    [XmlAttribute]
    public float end;
    [XmlAttribute]
    public int framerate;

    public string saveDir;
    public ConfigRecordingData savedData;

    public ConfigRecording()
    {
        start = 0;
        end = 0;
        framerate = 15;
    
        saveDir = ".\\Output\\";
        savedData = new ConfigRecordingData();
    }
}

/// <summary>
/// Recording configuration to be serialize in XML config
/// </summary>
public class ConfigRecordingData
{
    [XmlAttribute]
    public bool ImgOriginal;
    [XmlAttribute]
    public bool ImgSegmentation;
    [XmlAttribute]
    public bool ImgCategories;
    [XmlAttribute]
    public bool ImgDepth;
    [XmlAttribute]
    public bool ImgNormals;
    [XmlAttribute]
    public bool ImgOpticalFlow;
    [XmlAttribute]
    public bool BodyBoundingBoxes;
    [XmlAttribute]
    public bool HeadBoundingBoxes;

    public ConfigRecordingData()
    {
        ImgOriginal=true;
        ImgSegmentation = false;
        ImgCategories = false;
        ImgDepth = false;
        ImgNormals = false;
        ImgOpticalFlow = false;
        BodyBoundingBoxes = false;
        HeadBoundingBoxes = false;
    }
}

/// <summary>
/// Agents color configuration to be serialize in XML config
/// </summary>
public class ConfigAgentColor
{
    [XmlAttribute]
    public int firstAgent;
    [XmlAttribute]
    public int step;
    [XmlAttribute]
    public int lastAgent;

    [XmlAttribute]
    public float red;
    [XmlAttribute]
    public float green;
    [XmlAttribute]
    public float blue;

    public ConfigAgentColor()
    {
        firstAgent = 0;
        step = 1;
        lastAgent = -1;

        red = 1;
        green = 0;
        blue = 0;
    }

    public ConfigAgentColor(int f, int s, int l, float r, float g, float b)
    {
        firstAgent = f;
        step = s;
        lastAgent = l;

        red = r;
        green = g;
        blue = b;
    }
}
#endregion