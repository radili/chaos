/* Crowd Simulator Engine
** Copyright (C) 2018 - Inria Rennes - Rainbow - Julien Pettre
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**
** Authors: Julien Bruneau
**
** Contact: crowd_group@inria.fr
*/

using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using System;
using System.Collections.Generic;

/// <summary>
/// Main manager: Load the scene, create all the agents and manage users input
/// </summary>
public class LoadEnv : MonoBehaviour
{

#region attributes
    List<GameObject> avatars;                               // List of agents
    public GameObject cam;                                  // Scene camera
    private CamRecorder cam_Rec;                            // Control scene recording
    private CamMvt cam_Movement;                            // Control camera movement behavior
    private FiltersControl cam_fc;                          // Control camera filters
    private MenuManager menuM;								// Control the starting menu
    //private float rotSpeed = 5;                             

    // --------------------------
    // CAMERA MOVEMENT PARAMETERS
    const float camTranslationSpeed = 25.0f;                // Camera translation speed
    const float camShiftPower = 250.0f;                     // Shift effect on camera speed
    const float camMaxShiftPower = 1000.0f;                 // Maximun shift effect on camera speed
    const float camRotationSpeed = 0.25f;                   // Rotation speed
    Vector3 lastMouse = new Vector3(255, 255, 255);         // Last mouse position to check its movement
    float camShiftHold = 1.0f;                              // Control the effect of shift with holding time      

    List<GameObject> characters = new List<GameObject>();
    List<bool> characters_seen = new List<bool>();
    List<int> nseen_perframe = new List<int>();

#endregion


    /// <summary>
    /// Scene and agents initialization
    /// </summary>
    void Start()
    {
        avatars = new List<GameObject>();
        cam_Rec = cam.GetComponent<CamRecorder>();
        cam_Movement = cam.GetComponent<CamMvt>();
        cam_fc = cam.GetComponent<FiltersControl>();

        cam_Rec.enabled = false;
        cam_Movement.enabled = false;

        menuM = gameObject.GetComponent<MenuManager>();
    }

    /// <summary>
    /// Load a scenario: create stage and spawn agents
    /// </summary>
    /// <param name="trajDir">The path to the scenario file</param>
    public void loadScenario(string trajDir)
    {
        // --------------------------------------------------
        // SET RANDOM SEED TO HAVE SAME RESULTS AT EVERY RUNS
        UnityEngine.Random.InitState(75482);

        // ---------------------------------
        // INITIALIZE SCENE FROM CONFIG FILE

        // -------------
        // CAMERA CONFIG
        cam_Rec.enabled = false;
        cam_Movement.enabled = false;
        cam.transform.position = ConfigReader.camPosition;
        cam.transform.rotation = Quaternion.Euler(ConfigReader.camRotation);
        cam_Movement.lookAt_Id = ConfigReader.camLookAtTarget;
        cam_Movement.follow_Id = ConfigReader.camFollowTarget;
        cam_Movement.follow_LockX = !ConfigReader.camFollowOnX;
        cam_Movement.follow_LockZ = !ConfigReader.camFollowOnY;
        
        // -------------
        // RECORD CONFIG
        cam_Rec.record = ConfigReader.recording;
        cam_Rec.timeToStart = ConfigReader.recordingStart;
        cam_Rec.timeToStop = ConfigReader.recordingEnd;
        cam_Rec.framerate = ConfigReader.recordingFramerate;
        cam_fc.saveImage = ConfigReader.recordOriginalImg;
        cam_fc.saveIdSegmentation = ConfigReader.recordSegmentationImg;
        cam_fc.saveLayerSegmentation = ConfigReader.recordLayeredSegImg;
        cam_fc.saveDepth = ConfigReader.recordDepthImg;
        cam_fc.saveNormals = ConfigReader.recordNormalsImg;
        cam_fc.saveOpticalFlow = ConfigReader.recordOpticalFlowImg;



        // ---------------------
        // CLEAR PREVIOUS AGENTS
        foreach (GameObject a in avatars)
        {
            Destroy(a);
        }
        avatars.Clear();
    
        // -------------
        // CREATE AGENTS
        string expDir = "/home/robin/src/kriging/exp";
        string allDensDir = expDir + "/dens";
        string paramsFile = expDir + "/params";
        int [] params_ = getInts(paramsFile);
        int dens = params_[0];
        int nmotion = params_[1];
        int traj = params_[2];
        string densDir = allDensDir + "/" + dens;
        string nmotionDir = densDir + "/" + nmotion;
        string commonTrajDir = densDir + "/traj/" + traj;
        string indivTrajDir = nmotionDir + "/" + traj;
        string motionIdsFile = indivTrajDir + "/motionids";
        string charIdsFile = commonTrajDir + "/charids";
        string genderFile = commonTrajDir + "/genders";
        string agentsDir = commonTrajDir + "/agents";
        string synthParamsFile = expDir + "/synth_params_gen.csv";
        bool [] genders = getGenders(genderFile);
        int [] charIds = getInts(charIdsFile);
        int [] motionIds = getInts(motionIdsFile);

        cam_Rec.saveDir = "/home/robin/src/kriging/exp/screens/"+dens+"_"+nmotion+"_"+traj; 
        Camera.main.GetComponent<CamRecorder>().Init();
        InitDeepRecorders();
        
        DirectoryInfo dir = new DirectoryInfo(agentsDir);
        FileInfo[] info = dir.GetFiles("*.csv");
        if (info.Length == 0)
            info = dir.GetFiles("*.txt");
        var crowdParentObject = new GameObject("Crowd");
        FollowTrajectory tmpFollower;
        int i = 0;
        object testRocketMan = Resources.Load("Prefabs/RocketBox/male/prefab_light/LS_m001_light");
        foreach (FileInfo f in info)
        {
            // ------------------------------------------------------------
            // IF NO ROCKETMAN (PROPRIETARY ASSETS), USE PLASTICMAN INSTEAD
            // GameObject character = (testRocketMan!=null) ? CreateAgent_RocketBox(i) : CreateAgent_PlasticMan(i);
            GameObject character = CreateAgent_RocketBox_WithVar(charIds[i], genders[i]);
            // char_id = i % nchars;
            // GameObject character = characters.transform.GetChild(char_id).gameObject;
            // character.transform.parent = crowdParentObject.transform;
            // avatars.Add(character);
            characters.Add(character);
            characters_seen.Add(false);


            // ---------------------
            // SET NAME, TAG, AND ID
            character.name += i.ToString("D4");
            character.tag = "Player";
            character.GetComponent<GetMemberPosition>().ID = i;

            // ----------------------------------
            // FOLLOWTRAJECTORY SCRIPT MANAGEMENT
            // Setup of the CSV filename and disable the start synchro
            tmpFollower = character.GetComponent<FollowTrajectory>();
            tmpFollower.SetCsvFilename(f.FullName);
            tmpFollower._SyncLaunchWithTrajectory = true; // start the character at the csv time


            // get character anim
            string path = "Human_Animations/Generated/"+motionIds[i];
            AnimationClip characteranim = Resources.Load<AnimationClip>(path);
            characteranim.wrapMode = WrapMode.Loop;

            // -------------------------------
            // Load animation controller from ressources
            string PathAnimeController = "Human_Animations/controller";
            // character.AddComponent<Animator>();
            Animator animator = character.GetComponent<Animator>();
            RuntimeAnimatorController defaultcontroller = Resources.Load(PathAnimeController) as RuntimeAnimatorController;
            RuntimeAnimatorController charactercontroller = RuntimeAnimatorController.Instantiate(defaultcontroller);
            // AnimatorOverrideController oc = new AnimatorOverrideController(charactercontroller);
            // List<KeyValuePair<AnimationClip, AnimationClip>> overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>(oc.overridesCount);
            // oc.GetOverrides(overrides);
            // overrides[0] = new KeyValuePair<AnimationClip, AnimationClip>(overrides[0].Key, characteranim);
            // // overrides[1] = new KeyValuePair<AnimationClip, AnimationClip>(overrides[1].Key, overrides[1].Key);
            // oc.ApplyOverrides(overrides);
            // animator.runtimeAnimatorController = oc;
            AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(charactercontroller);
            animator.runtimeAnimatorController = animatorOverrideController;
            animatorOverrideController["character_animation"].wrapMode = WrapMode.Loop;
            animatorOverrideController["character_animation"] = characteranim;


            // animator.applyRootMotion = true;
            // make sure that object on the floor (height 0)
            // character.transform.position = new Vector3(character.transform.position.x, 0.0f, character.transform.position.z);
            i++;
        }

        cam_Movement.updateTargetedObject();
        cam_fc.OnSceneChange();

    }

    private List<string> readFile(string fileName){
        using(var reader = new StreamReader(fileName))
        {
            List<string> list = new List<string>();
            while (!reader.EndOfStream)
                list.Add(reader.ReadLine());
            return list;
        }
    }
    private bool[] getGenders(string paramsFile)
    {
        List<string> lines = this.readFile(paramsFile);
        bool [] genders = new bool[lines.Count];
        int i = 0;
        foreach (string line in lines){
            if(line.Length != 0){
                genders[i] = int.Parse(line) == 0;
                i++;
            }
        }
        return genders;
    }

    private int[] getInts(string fileName)
    {
        List<string> lines = this.readFile(fileName);
        int [] ints = new int[lines.Count];
        int i = 0;
        foreach (string line in lines){
            if(line.Length != 0){
                ints[i] = int.Parse(line);
                i++;
            }
        }
        return ints;
    }

    /// <summary>
    /// Manage which deep recorder is used and initialize it
    /// </summary>
    private void InitDeepRecorders()
    {
        GameObject DeepRecorders = GameObject.FindGameObjectWithTag("DeepRecorders");
        if (ConfigReader.recordBodyBoudingBoxes)
        {
            DeepRecorders.AddComponent<BodyBoundingBoxRecorder>();
            DeepRecorders.GetComponent<BodyBoundingBoxRecorder>().Init(ConfigReader.recordingSaveDir);
        }

        if (ConfigReader.recordHeadBoundingBoxes)
        {
            DeepRecorders.AddComponent<HeadBoundingBoxRecorder>();
            DeepRecorders.GetComponent<HeadBoundingBoxRecorder>().Init(ConfigReader.recordingSaveDir);
        }
    }

    Color? GetColorForAgentId(int id)
    {
        if (ConfigReader.agentsColor != null && ConfigReader.agentsColor.Count > 0)
        {
            foreach (ConfigAgentColor c in ConfigReader.agentsColor)
            {
                if (id > c.firstAgent - 1 && id < c.lastAgent + 1 && (id - c.firstAgent) % c.step == 0)
                    return new Color(c.red, c.green, c.blue, 1);
            }
        }
        return null;
    }

    Material CreateMaterialWithColor(Color color)
    {
        var tmpMaterial = new Material(Shader.Find("Legacy Shaders/Diffuse"));
        tmpMaterial.color = color;
        return tmpMaterial;
    }

    string GetModelName_RocketBox(int id)
    {
        string path;
        int modelId;
        if (id % 2 == 0)
        {
            path = "male/prefab_light/LS_m";
            modelId = id / 2;
        }
        else
        {
            path = "female/prefab_light/LS_f";
            modelId = (id - 1) / 2;
        }
        modelId = (modelId % 20) + 1;
        return "Prefabs/RocketBox/" + path + modelId.ToString("D3") + "_light";
    }

    GameObject CreateAgent_RocketBox_WithVar(int charId, bool isFemale){

        string folder = "RocketBox";
        string[] sexFolder = { "female" , "male"};

        int rockbxId = (charId/6)+1;
        int var_id = charId%6;
        // Use this for initialization
        GameObject temp;


        string genderString = isFemale ? "female" : "male";
        string actSex = genderString.Substring(0, 1);
        string actId = actSex + (rockbxId).ToString("D3");
        string characterPath = folder + "/"+ genderString + "/" + actId + "/" + actId + "_variation";

        // load character in Unity.
        UnityEngine.Object ro = Resources.Load(characterPath, typeof(GameObject));
        if(ro != null)
        { 
            temp = UnityEngine.Object.Instantiate(ro) as GameObject;

            TextureVariation tv = temp.GetComponentInChildren<TextureVariation>();
            tv.automaticVariationId = false;
            tv.InitialiseTextureVariation();
            tv.SetVariationId(var_id);

            temp.AddComponent<GetMemberPositionRocketBox>();
            temp.AddComponent<FollowTrajectory>();
            GameObject headBoundingBox = Instantiate(Resources.Load<GameObject>("HeadBoundingBoxes/Hbb_RocketBox"));
            Transform Head = temp.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 Head");
            headBoundingBox.transform.parent = Head;

            return temp;
        }
        else {
            Debug.Log("Could not load Character "+characterPath);
        }
        return null;
    }

    // GameObject CreateAgent_RocketBox(int id)
    // {
    //     // -------------------
    //     // LOAD AGENT IN UNITY
    //     string[] gender = avatars_[id%2];
    //     string fn = gender[id%gender.Length];
    //     string basename = fn.Split('/')[1];
    //     string path = "Human_Avatars/"+fn+"/Export/"+basename;
    //     Debug.Log(path);
    //     GameObject a = Resources.Load(path) as GameObject;
    //     GameObject gameObject = Instantiate(a);

    //     // -------------------
    //     // SET BOUNDING BOXES ITEM
    //     gameObject.AddComponent<GetMemberPositionRocketBox>();
    //     gameObject.AddComponent<FollowTrajectory>();
    //     GameObject headBoundingBox = Instantiate(Resources.Load<GameObject>("HeadBoundingBoxes/Hbb_RocketBox"));
    //     Transform Head = gameObject.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 Head");
    //     headBoundingBox.transform.parent = Head;

    //     // ---------------------------------------------
    //     // SET AGENT'S COLOR WHEN SPECIFY IN CONFIG FILE
    //     var tmpColor = GetColorForAgentId(id);
    //     if (tmpColor.HasValue)
    //     {
    //         var tmpMaterial = CreateMaterialWithColor(tmpColor.Value);
    //         Material[] mats = new Material[] { tmpMaterial, tmpMaterial, tmpMaterial };

    //         SkinnedMeshRenderer[] rendererList = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
    //         foreach (SkinnedMeshRenderer renderer in rendererList)
    //             renderer.materials = mats;
    //     }

    //     return gameObject;
    // }

    // GameObject CreateAgent_RocketBox1(int id)
    // {
    //     // -------------------
    //     // LOAD AGENT IN UNITY
    //     GameObject gameObject = Instantiate(Resources.Load<GameObject>(GetModelName_RocketBox(id)));

    //     // -------------------
    //     // SET BOUNDING BOXES ITEM
    //     gameObject.AddComponent<GetMemberPositionRocketBox>();
    //     gameObject.AddComponent<FollowTrajectory>();
    //     GameObject headBoundingBox = Instantiate(Resources.Load<GameObject>("HeadBoundingBoxes/Hbb_RocketBox"));
    //     Transform Head = gameObject.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 Head");
    //     headBoundingBox.transform.parent = Head;

    //     // ---------------------------------------------
    //     // SET AGENT'S COLOR WHEN SPECIFY IN CONFIG FILE
    //     var tmpColor = GetColorForAgentId(id);
    //     if (tmpColor.HasValue)
    //     {
    //         var tmpMaterial = CreateMaterialWithColor(tmpColor.Value);
    //         Material[] mats = new Material[] { tmpMaterial, tmpMaterial, tmpMaterial };

    //         SkinnedMeshRenderer[] rendererList = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
    //         foreach (SkinnedMeshRenderer renderer in rendererList)
    //             renderer.materials = mats;
    //     }

    //     return gameObject;
    // }

    // GameObject CreateAgent_PlasticMan(int id)
    // {
    //     // -------------------
    //     // LOAD AGENT IN UNITY
    //     GameObject gameObject = Instantiate(Resources.Load<GameObject>("PlasticMan/plasticman"));

    //     // -------------------
    //     // SET BOUNDING BOXES ITEM
    //     gameObject.AddComponent<GetMemberPositionPlasticMan>();
    //     GameObject headBoundingBox = Instantiate(Resources.Load<GameObject>("HeadBoundingBoxes/Hbb_PlasticMan"));
    //     Transform Head = gameObject.transform.Find("Plasticman:Reference/Plasticman:Hips/Plasticman:Spine/Plasticman:Spine1/Plasticman:Spine2/Plasticman:Spine3/Plasticman:Neck/Plasticman:Head");
    //     headBoundingBox.transform.parent = Head;

    //     // -----------------
    //     // SET AGENT'S COLOR
    //     var tmpColor = GetColorForAgentId(id);
    //     if (!tmpColor.HasValue)
    //         tmpColor = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value, 1);

    //     var tmpRenderer = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
    //     tmpRenderer.material = CreateMaterialWithColor(tmpColor.Value);

    //     return gameObject;
    // }

     private bool IsInView(GameObject toCheck)
     {
         Vector3 center = toCheck.transform.GetChild(0).gameObject.GetComponentInChildren<Renderer>().bounds.center;;
         Vector3 pointOnScreen = Camera.main.WorldToScreenPoint(center);
 
         //Is in FOV
         if ((pointOnScreen.x < 0) || (pointOnScreen.x > Screen.width) ||
                 (pointOnScreen.y < 0) || (pointOnScreen.y > Screen.height))
         {
             return false;
         }
         else{
             return true;
         }
     }

    /// <summary>
    /// Update camera state
    /// </summary>
    private void Update()
    {
        ConfigReader.camPosition = cam.transform.position;
        ConfigReader.camRotation = cam.transform.rotation.eulerAngles;
        int charcount = 0;
        int i = 0;
        bool allnull = true;
        foreach (GameObject character in characters){
            if(character != null && IsInView(character)){
                allnull = false;
                charcount++;
                characters_seen[i] = true;
            }
            i++;
        }
        if(allnull){
            int c = 0;
            foreach (bool b in characters_seen){
                if(b){
                    c++;
                }
            }
            float average = nseen_perframe.Sum()/nseen_perframe.Count;
            Debug.Log(c+" characters on screen since begining");
            Debug.Log(average+" average characters on screen per frame");
            Application.Quit();
        }
        else{
            Debug.Log(charcount+" characters on screen this frame");
            Debug.Log(Time.fixedTime);
            nseen_perframe.Add(charcount);
        }
    }

    /// <summary>
    /// Manage users Input
    /// </summary>
    void LateUpdate()
    {
        // -------------------
        // ESCAPE => EXIT APPS
        if (Input.GetKeyDown(KeyCode.Escape) == true)
        {
            menuM.toogleMenu();
        }

        // -------------------------------
        // MOUSE + RCLICK => ROTATE CAMERA
        if (Input.GetMouseButton(1))
        {
            lastMouse = Input.mousePosition - lastMouse;
            lastMouse = new Vector3(-lastMouse.y * camRotationSpeed, lastMouse.x * camRotationSpeed, 0);
            lastMouse = new Vector3(cam.transform.eulerAngles.x + lastMouse.x, cam.transform.eulerAngles.y + lastMouse.y, 0);

            // Block all the way up or down to prevent glitches
            if (lastMouse.x > 180 && lastMouse.x < 270)
                lastMouse.x = 270;
            else if (lastMouse.x > 90 && lastMouse.x < 180)
                lastMouse.x = 90;

            cam.transform.eulerAngles = lastMouse;
        }
        lastMouse = Input.mousePosition;

        // -----------------------------------------
        // ARROWS, SHIFT, CTRL => CAMERA TRANSLATION
        Vector3 p = new Vector3();
        if (Input.GetKey(KeyCode.UpArrow))
        {
            p += new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            p += new Vector3(0, 0, -1);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            p += new Vector3(-1, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            p += new Vector3(1, 0, 0);
        }
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            camShiftHold += Time.fixedUnscaledDeltaTime;
            p = p * Mathf.Max(camShiftHold * camShiftPower, camMaxShiftPower);
        }
        else
        {
            camShiftHold = Mathf.Clamp(camShiftHold * 0.5f, 1f, 1000f);
            p = p * camTranslationSpeed;
        }

        p = p * Time.fixedUnscaledDeltaTime;
        Vector3 newPosition = cam.transform.position;
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        { //If player wants to move on X and Z axis only
            cam.transform.Translate(p);
            newPosition.x = cam.transform.position.x;
            newPosition.z = cam.transform.position.z;
            cam.transform.position = newPosition;
        }
        else
        {
            cam.transform.Translate(p);
        }

        // --------------------------
        // F5 => SAVE CAMERA POSITION
        if (Input.GetKeyDown(KeyCode.F5))
        {
            menuM.saveConfig();
        }

        // ----------------------------
        // Cycle through filters F9 F10
        if (Input.GetKeyDown(KeyCode.F11))
        {
            cam_fc.cycleReset();
        }
        if (Input.GetKeyDown(KeyCode.F10))
        {
            cam_fc.cycleForward();
        }
        if (Input.GetKeyDown(KeyCode.F9))
        {
            cam_fc.cycleBackward();
        }

        // ----------------------
        // SPACE => PAUSE UNPAUSE
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.timeScale == 0)
                Time.timeScale = 1;
            else
                Time.timeScale = 0;
        }
    }
}
