﻿/* Crowd Simulator Engine
** Copyright (C) 2018 - Inria Rennes - Rainbow - Julien Pettre
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**
** Authors: Julien Bruneau
**
** Contact: crowd_group@inria.fr
*/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Starting menu manager
/// </summary>
public class MenuManager : MonoBehaviour
{

    string configPath;              // path to the selected scenario file
    ObstaclesReader obstReader;     // object loading/creating obstacles

    Camera cam;                     // the main camera
    InputField inputConfigPath;     // GUI object to input the scenario folder
    Dropdown configFilesMenu;       // GUI object to select the scenario
    GameObject menu;                // the menu gameObject

    /// <summary>
    /// initialize the menu
    /// </summary>
	void Start()
    {
        configPath = "/home/robin/build/UMANS/gen/ExampleFollow.xml";
        cam = Camera.main;
        cam.fieldOfView = 30;

        // menu = GameObject.FindGameObjectWithTag("Menu");
        obstReader = new ObstaclesReader();

        string dataPath = defaultScenarioPath();

        // inputConfigPath = menu.GetComponentInChildren<InputField>();
        // inputConfigPath.text = dataPath;

        // configFilesMenu = menu.GetComponentInChildren<Dropdown>();
        // updateConfigList(dataPath);
        startScenario();

        Time.timeScale = 0;

    }

    /// <summary>
    /// Build the default scenarios folder path from the application path
    /// </summary>
    /// <returns>default scenarios folder path</returns>
    public string defaultScenarioPath()
    {
        string pathPlayer = Application.dataPath;
        int lastIndex = pathPlayer.LastIndexOf('/');
        string dataPath = pathPlayer.Remove(lastIndex, pathPlayer.Length - lastIndex);

        string scenarioPath = dataPath + "/Scenarios";

        if (Directory.Exists(scenarioPath))
            return scenarioPath;

        return dataPath;
    }

    /// <summary>
    /// update the dropdown menu listing the scenarios
    /// </summary>
    /// <param name="NewPath">path to the current scenario folder</param>
    public void updateConfigList(string NewPath)
    {
        if (configFilesMenu == null)
            return;

        DirectoryInfo dir;
        if (Directory.Exists(inputConfigPath.text))
            dir = new DirectoryInfo(inputConfigPath.text);
        else
        {
            string dataPath = defaultScenarioPath();

            dir = new DirectoryInfo(dataPath);
        }
        FileInfo[] infos = dir.GetFiles("*.xml");
        configFilesMenu.options.Clear();
        configFilesMenu.options.Add(new Dropdown.OptionData("NONE"));
        foreach (FileInfo i in infos)
        {
            configFilesMenu.options.Add(new Dropdown.OptionData(i.Name.Remove(i.Name.Length - 4)));
        }
        configFilesMenu.value = 0;
        configFilesMenu.RefreshShownValue();
    }

    /// <summary>
    /// Update the current scenario file
    /// </summary>
    /// <param name="i">the id of the selection from the dropdown menu</param>
    public void updateConfigFile(int i)
    {
        if (configFilesMenu == null || configFilesMenu.value == 0)
            return;

        configPath = inputConfigPath.text + '/' + configFilesMenu.options[configFilesMenu.value].text + ".xml";

        //cam.transform.position = ConfigReader.camPosition;
        //cam.transform.rotation = Quaternion.Euler(ConfigReader.camRotation);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Load the current scenario file and remove the menu
    /// </summary>
    public void startScenario()
    {
        if (configPath == null )
            return;

        ConfigReader.LoadConfig(configPath);
        obstReader.clear();
        obstReader.createObstacles(ConfigReader.obstaclesFile, ConfigReader.stageInfos);

        LoadEnv env = gameObject.GetComponent<LoadEnv>();
        env.loadScenario(ConfigReader.trajectoriesDir);
        menu.SetActive(false);
    }

    /// <summary>
    /// show/hide menu
    /// </summary>
    public void toogleMenu()
    {
        menu.SetActive(!menu.activeSelf);
    }

    /// <summary>
    /// save the current scenario file
    /// </summary>
    public void saveConfig()
    {
        ConfigReader.SaveConfig(configPath);
    }

    /// <summary>
    /// quit the application
    /// </summary>
    public void exit()
    {
        Application.Quit();
    }
}