using UnityEditor;
using UnityEngine;
using UnityEditor.Animations;
using System.IO;
using System.Collections.Generic;
using System;
public class ChangeModelToHumanoid : AssetPostprocessor
{
    const string PARAMSFILE = "/home/robin/src/kriging/exp/synth_params_gen.csv";
     void OnPreprocessModel()
     {
         if (assetPath.Contains("Human_Animations/Generated"))
         {
             ModelImporter importer = (ModelImporter)assetImporter;
 
            //Switch to humanoid
            importer.animationType = ModelImporterAnimationType.Human;
            importer.avatarSetup = ModelImporterAvatarSetup.CopyFromOther;
            int fileId = int.Parse(Path.GetFileName(assetPath).Split('.')[0]);
            // string avatarPath = "Human_Animations/Avatars";
            string avatarPath = "";
            if (this.isFemale(PARAMSFILE, fileId)){
                avatarPath = "RocketBox/female/f005/f005";
                // avatarPath += "/femaleskeleton";
            }else{
                avatarPath = "RocketBox/male/m008/m008";
                // avatarPath += "/maleskeleton";
            }
            Avatar avatar = Resources.Load<Avatar>(avatarPath);
            importer.sourceAvatar = avatar;
         }
     }

    bool isFemale(string paramsFile, int fileId){
        using(var reader = new StreamReader(paramsFile))
        {
            List<string> listA = new List<string>();
            string [] params_ = reader.ReadLine().Split(',');
            int genderId = Array.IndexOf(params_, "gender");
            for (int i = 0; i < fileId; i++)
                reader.ReadLine();
            return Math.Round(float.Parse(reader.ReadLine().Split(',')[genderId])) == 0;
        }
    }
 
     void OnPostprocessModel(GameObject model)
     {
         if (assetPath.Contains("Human_Animations"))
         {
            ModelImporter importer = (ModelImporter)assetImporter;
             
            ModelImporterClipAnimation[] clipAnimations = new ModelImporterClipAnimation[] {new ModelImporterClipAnimation()};

            // maybe the worst widely used language and software ever
            clipAnimations[0].events = importer.defaultClipAnimations[0].events;
            clipAnimations[0].heightFromFeet = importer.defaultClipAnimations[0].heightFromFeet;
            clipAnimations[0].heightOffset = importer.defaultClipAnimations[0].heightOffset;
            // clipAnimations[0].keepOriginalOrientation = importer.defaultClipAnimations[0].keepOriginalOrientation;
            // clipAnimations[0].keepOriginalPositionXZ = importer.defaultClipAnimations[0].keepOriginalPositionXZ;
            // clipAnimations[0].keepOriginalPositionY = importer.defaultClipAnimations[0].keepOriginalPositionY;
            clipAnimations[0].keepOriginalOrientation = true;
            clipAnimations[0].keepOriginalPositionXZ = true;
            clipAnimations[0].keepOriginalPositionY = true;
            clipAnimations[0].lockRootHeightY = true;
            clipAnimations[0].lockRootPositionXZ = true;
            clipAnimations[0].lockRootRotation = true;
            // clipAnimations[0].lockRootHeightY = importer.defaultClipAnimations[0].lockRootHeightY;
            // clipAnimations[0].lockRootPositionXZ = importer.defaultClipAnimations[0].lockRootPositionXZ;
            // clipAnimations[0].lockRootRotation = importer.defaultClipAnimations[0].lockRootRotation;
            clipAnimations[0].loopPose = importer.defaultClipAnimations[0].loopPose;
            clipAnimations[0].maskSource = importer.defaultClipAnimations[0].maskSource;
            clipAnimations[0].maskType = importer.defaultClipAnimations[0].maskType;
            clipAnimations[0].mirror = importer.defaultClipAnimations[0].mirror;
            clipAnimations[0].rotationOffset = importer.defaultClipAnimations[0].rotationOffset;
            clipAnimations[0].takeName = importer.defaultClipAnimations[0].takeName;
            clipAnimations[0].curves = importer.defaultClipAnimations[0].curves;
            clipAnimations[0].name = importer.defaultClipAnimations[0].name;
            // clipAnimations[0].firstFrame = importer.defaultClipAnimations[0].firstFrame;
            clipAnimations[0].firstFrame = 0;
            clipAnimations[0].lastFrame = importer.defaultClipAnimations[0].lastFrame;
            clipAnimations[0].loop = importer.defaultClipAnimations[0].loop;
            clipAnimations[0].wrapMode = importer.defaultClipAnimations[0].wrapMode;
            clipAnimations[0].loopTime = true;
            clipAnimations[0].cycleOffset = 0;
            importer.clipAnimations = clipAnimations;
            // importer.clipAnimations = importer.defaultClipAnimations;
            //Add the animator and preconfigure it.
            //  if (!model.GetComponent<Animator>())
            //  {
            //      model.AddComponent<Animator>();
            //      model.GetComponent<Animator>().avatar = importer.sourceAvatar;
            //      model.GetComponent<Animator>().applyRootMotion = true;
            //  }
            // importer.SaveAndReimport();
            // AssetDatabase.ImportAsset(assetPath);
            // var clip = (AnimationClip) AssetDatabase.LoadAssetAtPath(assetPath, typeof(AnimationClip));
            // var animatorpath=assetPath.Split('.')[0]+".controller";
            // FileUtil.CopyFileOrDirectory("Assets/Resources/PlasticMan/Animation/LocomotionFinal 1.controller", animatorpath);
            // UnityEditor.AssetDatabase.SaveAssets();
            // UnityEditor.AssetDatabase.Refresh();
            // // Debug.Log("Copy Success");
            // Animator controller = AssetDatabase.LoadAssetAtPath<Animator>(animatorpath);
            // foreach (var state in controller.layers[0].stateMachine.states){
            //     if (state.state.name == "Marche"){
            //         state.state.motion = clip;
                // }
            // }
         }
     }

}